# Week 1

[x] Challenge Passed?

## Challenges

- **Primary Challenge**
  - Simple application that takes an input, sends a GET request and returns the full name of a person.
  - [Link](./PrimaryChallenge)
- **Advanced Variations**
  - Related People
    - Retrieve the related people for a person based on their ID
    - Unsuccessful due to their being a requirement for a logged in user in order to access this data
    - [Link](./AdvancedVariations/RelatedPeople)
