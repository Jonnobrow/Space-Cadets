import java.io.*;
import java.net.*;
import java.util.regex.*;

public class ECSRelatedPeopleFinder {

  public static void main(String[] args) throws Exception {
    // Get the id of the person to find related people for
    BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
    String enteredUserId = inputReader.readLine();
    inputReader.close();
    // Make instance of class
    ECSRelatedPeopleFinder peopleFinder = new ECSRelatedPeopleFinder();
    // Get the array of people
    String[] relatedPeople = peopleFinder.requestPeopleFromWebPage(enteredUserId);
    // Loop through array and output
    for (int i = 0; i < 1000; i ++) {
      if (relatedPeople[i] != "" && relatedPeople[i] != null)
        System.out.println(relatedPeople[i]);
    }
  }

  public String[] requestPeopleFromWebPage(String id) {
    String[] namesList = new String[1000];
    try {
      // Make request to server
      URL url = new URL(String.format("https://secure.ecs.soton.ac.uk/people/%s/related_people", id));
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("GET");
      int status = connection.getResponseCode();
      // Check for success
      if (status == 200) {
        // Create a buffered reader to process the response stream
        BufferedReader webResponseReader = new BufferedReader(
          new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer responseContent = new StringBuffer();
        while ((inputLine = webResponseReader.readLine()) != null) {
          responseContent.append(inputLine);
        }
        webResponseReader.close();
        // Get the names from the StringBuffer
        int startLookingAt = responseContent.indexOf("<!-- Main Page Begins -->");
        int stopLookingAt = responseContent.indexOf("<!-- Main Page Ends -->");
        String stringToSearch = responseContent.toString().substring(startLookingAt,stopLookingAt);
        String regexString = ">([a-zA-Z\\s]*?)<\\/a>";
        Pattern pattern = Pattern.compile(regexString);
        Matcher matcher = pattern.matcher(stringToSearch);
        int namesListIndex = 0;
        while(matcher.find()){
          namesList[namesListIndex] = matcher.group(1);
          namesListIndex ++;
        }
      }
      // Disconnect from the connection
      connection.disconnect();
    } catch(Exception e) {
      e.printStackTrace();
    }
    return namesList;
  }

}
