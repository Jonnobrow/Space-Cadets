# Primary Challenge

**Build a simple application in Java that retrieves the full name of a person based on their Southampton University ID**

The application should take the input of an ID from the user and then send a GET request to `https://www.ecs.soton.ac.uk/people/{id}`. Following this it should parse the HTML response and retrieve the name which will lie between `property="name">` and `</h1>`.
