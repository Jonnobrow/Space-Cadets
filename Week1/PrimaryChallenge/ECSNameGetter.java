import java.io.BufferedReader; // Used for input
import java.io.InputStreamReader; // Used for input
import java.net.HttpURLConnection; // Used for web request
import java.net.URL; // Used for web request
import java.util.regex.Pattern; // Used for matching string
import java.util.regex.Matcher; // Used for mathcing string

/**
 * This class contains all of the functionality to take an input from the user
 * and query the 'ecs.soton.ac.uk/people/' webpage for the related name
 */
public class ECSNameGetter {

  /**
   * This is the base url which the id must be appended to before querying
   */
  private String baseUrl = "https://www.ecs.soton.ac.uk/people";

  public static void main(String[] args) throws Exception {
    // Output introduction text using a BufferedWriter
    System.out.println("Enter the id of the user to search for: ");
    // Create a new BufferedReader on the System.in stream
    BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
    String enteredUserId = inputReader.readLine();
    inputReader.close();
    // Create an instance of the ECSNameGetter class
    ECSNameGetter nameGetter = new ECSNameGetter();
    // Get the name from the id
    String name = nameGetter.getNameFromId(enteredUserId);
    name = name != "" ? name : "Failed to get name!";
    // Output Name
    System.out.println(name);
  }

  /**
   * Get the name of the person based on their id
   * @param  id        The id of the person to search for
   * @return           The string containing a persons name or nothing depending on success
   */
  public String getNameFromId(String id) {
    String name = "";
    try {
      URL url = new URL(String.format("%s/%s", baseUrl, id));
      // Setup the HttpURLConnection
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      // Set the request method
      connection.setRequestMethod("GET");
      // Execute the request and get the response code
      int status = connection.getResponseCode();
      // Check for success
      if (status == 200) {
        // Create a buffered reader to process the response stream
        BufferedReader webResponseReader = new BufferedReader(
          new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer responseContent = new StringBuffer();
        while ((inputLine = webResponseReader.readLine()) != null) {
          responseContent.append(inputLine);
        }
        webResponseReader.close();
        // Get the name from the StringBuffer
        String regexString = "\"name\">(.*)<\\/h1";
        Pattern pattern = Pattern.compile(regexString);
        Matcher matcher = pattern.matcher(responseContent.toString());
        while(matcher.find()){
          name = matcher.group(1);
        }
      }
      // Disconnect from the connection
      connection.disconnect();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return name;
  }
}
