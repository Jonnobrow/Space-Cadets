# Space Cadets Challenges Repository

### Challenge Webpage
[Link](https://secure.ecs.soton.ac.uk/student/wiki/w/COMP1202/Space_Cadets)

### Contents
* [Week 1](./Week1)
  - [Primary Challenge](./Week1/PrimaryChallenge)
  - [Advanced Variations](./Week1/AdvancedVariations)
    - [Related People](./Week1/RelatedPeople)
* [Week 2](./Week2)
  - [Primary Challenge](./Week2/PrimaryChallenge)
