import java.io.*;

/**
 * @author Jonathan Bartlett <jsb1g18@soton.ac.uk>
 * BareBonesInterpreterMain
 */
public class BareBonesInterpreterMain {

  public static void main(String[] args) {

    File bareBonesFile = new File("");

    if (args.length > 0 && args[0] != null) {
      bareBonesFile = new File(args[0]);
    } else {
      System.out.println("No Bare Bones File Provided!");
      System.exit(0);
    }

    Interpreter interpeter = new Interpreter(bareBonesFile);
    while(!interpeter.interpretComplete) {
      interpeter.doNext();
      System.out.println(interpeter.getState());
    }
    
  }

}
