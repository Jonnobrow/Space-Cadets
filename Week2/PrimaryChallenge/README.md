# Primary Challenge

**Build an interpreter for Bare Bones**

## Assumptions Made For The Interpreter
- A variable is defined using `clear $varName$;` and any reference to a variable with the name prior to this statement will result in an error.
- The condition for a while loop is always `not 0`

This interpreter should be able to execute the following Bare Bones scripts:

```
clear X;
incr X;
incr X;
incr X;
while X not 0 do;
   decr X;
end;
```

and

```
clear X;
incr X;
incr X;
clear Y;
incr Y;
incr Y;
incr Y;
clear Z;
while X not 0 do;
   clear W;
   while Y not 0 do;
      incr Z;
      incr W;
      decr Y;
   end;
   while W not 0 do;
      incr Y;
      decr W;
   end;
   decr X;
end;
```

## Useful Sources
[How to write a simple interpreter](https://ruslanspivak.com/lsbasi-part1/)
