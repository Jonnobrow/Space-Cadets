import java.io.*;
import java.util.*;

/**
 * @author Jonathan Bartlett <jsb1g18@soton.ac.uk>
 * Interpreter
 * The actual interpreter for the Bare Bones language
 */
public class Interpreter {

  public Boolean interpretComplete = false;

  // Memory Object Being Used
  private Memory memory = new Memory();

  /**
   * This constructs the Interpreter
   * @param memory The memory object for this interpreter
   */
  public Interpreter(File file) {
    try {
      Scanner fileScanner = new Scanner(file);
      int line = 0;
      while (fileScanner.hasNext()) {
        memory.Lines.put(line, tokeniseLine(fileScanner.nextLine()));
        line ++;
      }
      fileScanner.close();
    } catch (FileNotFoundException e) {
        e.printStackTrace();
        System.out.println("Invalid File!");
        System.exit(0);
    }
    memory.CurrentLine = 0;
  }

  /**
   * Gets the state of the Interpreter at a specific point
   * @return A string containing variables, current line and the while loop stack
   */
  public String getState() {
    String state = interpretComplete ? "Final State:\n" : "Current State:\n";
    state += "Variables: " + memory.Variables + "\n";
    state += "Current Line: " + memory.CurrentLine + "\n";
    state += "While Loop Stack: " + memory.NestingStack + "\n";
    return state;
  }

  /**
   * Carry out the next operation of the interpreter
   */
  public void doNext() {
    if (memory.CurrentLine == memory.Lines.size()) {
      System.out.println("Execution Completed!");
      interpretComplete = true;
    } else {
      processLine();
    }
  }

  /** This converts a single line into tokens */
  private Token[] tokeniseLine(String line) {
    String[] tokenStrings = line.trim().replaceAll(" +", " ").split("(( )|(?<=;)|(?=;))", 0);
    Token[] tokens = new Token[tokenStrings.length];
    for (int tokenIndex = 0; tokenIndex < tokenStrings.length; tokenIndex++) {
      tokens[tokenIndex] = new Token(tokenStrings[tokenIndex]);
      if (!tokens[tokenIndex].isValidToken())
        error(String.format("Invalid Token: '%s' at line %d", tokenStrings[tokenIndex], memory.CurrentLine));
    }
    return tokens;
  }

  /**
   * Process the tokens in the tokens array
   * @return The next line number to interpret
   */
  private void processLine() {

    Integer nextLineNumber = memory.CurrentLine + 1;

    switch(memory.getCurrentLine()[0].tokenType) {
      case CLEAR:
        clearCommand();
        break;
      case INCR:
        incrementDecrementCommand(1);
        break;
      case DECR:
        incrementDecrementCommand(-1);
        break;
      case WHILE:
        memory.NestingStack.push(memory.CurrentLine);
        nextLineNumber = whileCommand();
        break;
      case EOB:
        nextLineNumber = memory.NestingStack.pop();
        break;
      default:
        break;
    }

    memory.CurrentLine = nextLineNumber;
  }

  /**
   * The command relating to the `CLEAR` token
   */
  private void clearCommand() {
    if (isLineValid("clear")) {
      String varName = memory.getCurrentLine()[1].variableName;
      memory.Variables.put(varName, 0);
    } else {
      error(String.format("Line %d is invalid!", memory.CurrentLine));
    }  
  }

  /**
   * The command realating to the `Increment` token
   */
  private void incrementDecrementCommand(int change) {
    if (isLineValid("increment") || isLineValid("decrement")) {
      String varName = memory.getCurrentLine()[1].variableName;
      if (memory.Variables.containsKey(varName))
        memory.Variables.put(varName, memory.Variables.get(varName) + change);
      else
        error(String.format("Variable '%s' Does Not Exist!", varName));
    } else {
      error(String.format("Line %d is invalid!", memory.CurrentLine));
    }
  }

  /**
   * The command realating to the `While` token
   */
  private int whileCommand() {
    String variableName = memory.getCurrentLine()[1].variableName;
    int nextLineNumber = memory.CurrentLine + 1;
    System.out.println(getState());
    if (isLineValid("while") && 
        memory.Variables.containsKey(variableName) && 
        memory.Variables.get(variableName) <= 0) 
    {
      int endCount = 0, whileCount = 1;
      for (int lineNumber = memory.CurrentLine + 1; lineNumber < memory.Lines.size(); lineNumber++) {
        Token firstTokenInLine = memory.Lines.get(lineNumber)[0];
        if (firstTokenInLine.tokenType == Token.TokenType.EOB && whileCount == endCount + 1) {
          memory.NestingStack.pop();
          return lineNumber + 1;
        } else if (firstTokenInLine.tokenType == Token.TokenType.EOB) {
          endCount ++;
        } else if (firstTokenInLine.tokenType == Token.TokenType.WHILE) {
          whileCount ++;
        }
      }
      if (endCount != whileCount) 
        error("Failed to find 'end' to 'while' block");
    }
    return nextLineNumber;
  }

  /**
   * On Error
   */
  private void error(String msg) {
    System.out.println("ERROR: " + msg);
    System.exit(0);
  }

  /**
   * Checks the validity of the line based on the given pattern
   * @param The name of the pattern as specified in the linePatterns hashmap
   * @return Whether of not this line is valid
   */
  private Boolean isLineValid(String patternName) {
    int matches = 0;
    for (int tokenIndex = 0; tokenIndex < memory.getCurrentLine().length; tokenIndex++) {
      if (memory.getCurrentLine()[tokenIndex].tokenType == linePatterns.get(patternName)[tokenIndex] && matches != tokenIndex + 1) {
        matches++;
      }
    }
    return matches == memory.getCurrentLine().length;
  }

  /**
   * This hash map contains all of the patterns allowed
   */
  private static final HashMap<String,Token.TokenType[]> linePatterns; 
  static {
    linePatterns = new HashMap<String,Token.TokenType[]>();
    linePatterns.put("increment",new Token.TokenType[] {Token.TokenType.INCR, Token.TokenType.VAR, Token.TokenType.ENDLINE});
    linePatterns.put("decrement",new Token.TokenType[] {Token.TokenType.DECR, Token.TokenType.VAR, Token.TokenType.ENDLINE});
    linePatterns.put("clear",new Token.TokenType[] {Token.TokenType.CLEAR, Token.TokenType.VAR, Token.TokenType.ENDLINE}); 
    linePatterns.put("while",new Token.TokenType[] {Token.TokenType.WHILE, Token.TokenType.VAR, Token.TokenType.NOT, Token.TokenType.ZERO, Token.TokenType.DO, Token.TokenType.ENDLINE}); 
    linePatterns.put("endBlock",new Token.TokenType[] {Token.TokenType.EOB, Token.TokenType.ENDLINE});  
  }

}
