import java.util.*;

/**
 * @author Jonathan Bartlett <jsb1g18@soton.ac.uk>
 * Token
 * This is used to tokenise the lines of code in Bare Bones script in order to use them easily
 */
public class Token {

  // All of the possible token types
  public enum TokenType {
    CLEAR, INCR, DECR, VAR, WHILE, ZERO, NOT, DO, ENDLINE, EOB
  }

  // The type of token
  public TokenType tokenType;

  // The variable name if TokenType=VAR
  public String variableName;

  // Constructor for VAR
  public Token(String tokenToParse) {
    tokenType = stringToTokenMap.get(tokenToParse) != null ? stringToTokenMap.get(tokenToParse) : null;
    if (tokenType == null && tokenToParse.matches("^[a-zA-Z0-9]*$")) {
      tokenType = TokenType.VAR;
      variableName = tokenToParse;
    }
  }

  public Boolean isValidToken() {
    return tokenType != null;
  }

  private static final HashMap<String, TokenType> stringToTokenMap;
  static {
    stringToTokenMap = new HashMap<String, TokenType>();
    stringToTokenMap.put("clear", TokenType.CLEAR);
    stringToTokenMap.put("incr", TokenType.INCR);
    stringToTokenMap.put("decr", TokenType.DECR);
    stringToTokenMap.put("var", TokenType.VAR);
    stringToTokenMap.put("while", TokenType.WHILE);
    stringToTokenMap.put("0", TokenType.ZERO);
    stringToTokenMap.put("not", TokenType.NOT);
    stringToTokenMap.put("do", TokenType.DO);
    stringToTokenMap.put(";", TokenType.ENDLINE);
    stringToTokenMap.put("end", TokenType.EOB);
  }

}
