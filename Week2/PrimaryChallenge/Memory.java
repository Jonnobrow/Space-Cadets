import java.util.HashMap;
import java.util.Stack;

/**
 * Memory 
 * Stores the state and variables for the current program being interpreted 
 * @author Jonathan Bartlett <jsb1g18@soton.ac.uk>
 */
public class Memory {

  // Stores the variables and their corresponding values
  public HashMap<String, Integer> Variables = new HashMap<String, Integer>();

  // Stores the current line being interpreted
  public int CurrentLine = 0;

  // Nesting stack, 0th element is the line at the top of the current loop
  public Stack<Integer> NestingStack = new Stack<Integer>();

  // Programmed loaded as an array of lines
  public HashMap<Integer,Token[]> Lines = new HashMap<Integer, Token[]>();
  
  // Gets current line
  public Token[] getCurrentLine() {
    return Lines.get(CurrentLine);
  }

}
